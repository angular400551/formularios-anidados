import { Injectable } from '@angular/core';
import { Country, Region, SmallCountry } from '../interfaces/country.interface';
import { Observable, combineLatest, map, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  // https://restcountries.com/v3.1/region/europe?fields=name,cca3,borders
  // https://restcountries.com/v3.1/alpha/co?fields=name,cca3,borders
  private baseUrl:string='https://restcountries.com/v3.1'

  private _regions:Region[]=[Region.Africa,Region.Americas,Region.Asia,Region.Europe,Region.Oceania];
  
  constructor(private http:HttpClient) { }

  get regions():Region[]{
    // console.log({...this._regions});
    return [...this._regions];  
  }

  getCountriesByRegion(region:Region):Observable<SmallCountry[]>{
    if(!region) return of([]);
    const url = `${this.baseUrl}/region/${region}?fields=name,cca3,borders`
    return this.http.get<Country[]>(url)
               .pipe(
                map(countries=> countries.map(country=>({
                  name:country.name.common,
                  cca3:country.cca3,
                  borders:country.borders ?? []
                })))
               )

  }

  getCountryByAlphacode(code:string):Observable<SmallCountry>{
    const url=`${this.baseUrl}/alpha/${code}?fields=name,cca3,borders`;
    if(!code) return of ();
    return this.http.get<Country>(url)
               .pipe(
                map(country=> ({
                  name:country.name.common,
                  cca3:country.cca3,
                  borders:country.borders ?? []
                }))
                )               
  }

  getCountryBordersByCodes(borders:string[]):Observable<SmallCountry[]>{
    if(!borders || borders.length===0) return of ([])
    const countryRequest:Observable<SmallCountry>[]=[]
    borders.forEach(code=>{
      const request= this.getCountryByAlphacode(code);
      countryRequest.push(request);
    });
    return combineLatest(countryRequest); 
  }
}
